# DWH - Lab

## Your challenge

Your challenge is to create a load the kddcup.data_10_percent.gz dataset into a cloud storage bucket 
and analyze the data with a pyspark job launched from a notebook (spark_on_cloud_hadoop.ipynb).

## Create a cloud storage bucket and upload the data

Create a cloud storage bucket and upload the file kddcup.data_10_percent.gz

## Create a hadoop cluster

From GCP Dataproc Console click on "Create cluster"

![](images/step1.png)

Name your cluster and check "Enable acces to the web interfaces"

![](images/step2.png)

Click on "Advanced options", In "Image" choose Cloud Dataproc version 1.4.
Select optional components Anaconda and Jupyter.

![](images/step3.png)

Click on Create.

![](images/step4.png)

## Launch pyspark jobs to analyze the data

Once your cluster is ready, in the "Web Interfaces" tab click on JupyterLab.

![](images/step5.png)

Drag and drop spark_on_cloud.ipynb and make sure the kernel used is PySpark.
Replace the BUCKET variable with the name of your bucket
Replace the STORAGE variable with "gs"

![](images/step11.png)

Execute all the cells and verify that the results of your analysis are in your s3 bucket

## Clean up

Don't forget to delete your cluster and your cloud storage bucket!


