## Create a new project

    export PROJECT_ID=<choose a globally unique name e.g. lab-paas-1287612946219>

    gcloud projects create $PROJECT_ID
    
    gcloud config set project $PROJECT_ID

## App Engine

Run the following command to install the gcloud component that includes the App Engine extension for Python 3.7:

    gcloud components install app-engine-python

### Run Hello World on your local machine

To run the Hello World app on your local computer:

    Create an isolated Python environment in a directory external to your project and activate it:

    python3 -m venv env
    source env/bin/activate

Navigate to your project directory and install dependencies:

    cd hello-world-flask
    pip install  -r requirements.txt

Run the application:

    python application.py

In your web browser, enter the following address:

http://localhost:8080

### Deploy and run Hello World on App Engine
To deploy your app to the App Engine standard environment:

Deploy the Hello World app by running the following command:

    gcloud app deploy

Launch your browser to view the app at https://PROJECT_ID.REGION_ID.r.appspot.com where PROJECT_ID represents your Google Cloud project ID.

    gcloud app browse

This time, the page that displays the Hello World message is delivered by a web server running on an App Engine instance.

## Delete the Project

    gcloud projects delete $PROJECT_ID
    
    gcloud config set project <my default project id>