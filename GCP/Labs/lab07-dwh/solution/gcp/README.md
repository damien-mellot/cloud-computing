# DB - Lab

## Your challenge

Your challenge is to create a load the nyc_tlc_yellow_trips_2018_subset_1.csv dataset into a cloud datawarehouse and analyze the data.

# Creat a cloud storage bucket and upload the data

## Create a new dataset to store tables

In the BigQuery console, click on the name of your project, then click Create Dataset.

![](images/step1.png)

Set the Dataset ID to nyctaxi. Leave the other fields at their default values.

![](images/step2.png)

Click Create dataset.

You'll now see the nyctaxi dataset under your project name.

## Ingest a new Dataset from a CSV

In the BigQuery Console, Select the nyctaxi dataset then click Create Table

![](images/step3.png)

Specify the below table options:

Source:

Create table from: Upload
Choose File: select the file you downloaded locally earlier
File format: CSV
Destination:

Table name: 2018trips Leave all other setting at default.
Schema:

Check Auto Detect (tip: Not seeing the checkbox? Ensure the file format is CSV and not Avro)

Advanced Options : Leave at default values

Click Create Table.


![](images/step4.png)


You should now see the 2018trips table below the nyctaxi dataset.

![](images/step5.png)

Select Preview and confirm all columns have been loaded (sampled below)

![](images/step6.png)

## Analyze the data

practice with a basic query on the 2018trips table

Verify that your processing location is EU:

![](images/step7.png)
![](images/step8.png)

In the Query Editor, write a query to list the top 5 most expensive trips of the year

    SELECT
      *
    FROM
      nyctaxi.2018trips
    ORDER BY
      fare_amount DESC
    LIMIT  5

## Ingest a new Dataset from Cloud Storage

Now, lets try load another subset of the same 2018 trip data that is available on Cloud Storage. And this time, let's use the CLI tool to do it.

In your Cloud Shell, run the following command

    bq load \
    --source_format=CSV \
    --autodetect \
    --noreplace  \
    nyctaxi.2018trips \
    gs://lab_dwh/nyc_tlc_yellow_trips_2018_subset_2.csv
    
Back on your BigQuery console, select the 2018trips table and view details. Confirm that the row count has now almost doubled.

You may want to run the same query like earlier to see if the top 5 most expensive trips have changed.

## Create tables from other tables with DDL

The 2018trips table now has trips from throughout the year. What if you were only interested in January trips? For the purpose of this lab, we will keep it simple and focus only on pickup date and time. Let's use DDL to extract this data and store it in another table

In the Query Editor, run the following CREATE TABLE command

    CREATE TABLE
      nyctaxi.january_trips AS
    SELECT
      *
    FROM
      nyctaxi.2018trips
    WHERE
      EXTRACT(Month
      FROM
        pickup_datetime)=1;
        
Now run the below query in your Query Editor find the longest distance traveled in the month of January

    SELECT
      *
    FROM
      nyctaxi.january_trips
    ORDER BY
      trip_distance DESC
    LIMIT
      1

## Clean up

Don't forget to delete your dataset and cloud storage bucket!
