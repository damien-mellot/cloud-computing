# Training and prediction with Keras and Google Cloud AI Platform

based on https://github.com/GoogleCloudPlatform/cloudml-samples/tree/master/census

This code implements a Logistic Regression model using the Google Cloud Platform. 
It includes code to process data, train a tensorflow model with hyperparameter tuning, run predictions on new data and assess model performance.

The sample provided in TensorFlow Keras uses tf.keras, TensorFlow's implementation of the Keras API specification. 
This provides the benefits of Keras and also first-class support for TensorFlow-specific functionality.

This lab walk through training a model on AI Platform using prewritten Keras code, deploying the trained model to AI Platform, and serving online predictions from the deployed model.

## Dataset

This tutorial uses the [United States Census Income Dataset](https://archive.ics.uci.edu/ml/datasets/census+income) provided by the [UC Irvine Machine Learning Repository](https://archive.ics.uci.edu/ml/index.php). This dataset contains information about people from a 1994 Census database, including age, education, marital status, occupation, and whether they make more than $50,000 a year.

The code will automatically download the train and eval datasets but you can browse the data in the data repository.


## Objective

The goal is to train a deep neural network (DNN) using Keras that predicts whether a person makes more than $50,000 a year (target label) based on other Census information about the person (features).


## Create a Cloud Storage bucket

When you submit a training job using the Cloud SDK, you upload a Python package containing your training code to a Cloud Storage bucket. AI Platform runs the code from this package. In this tutorial, AI Platform also saves the trained model that results from your job in the same bucket. You can then create an AI Platform model verison based on this output in order to serve online predictions.

Set the name of your Cloud Storage bucket as an environment variable. It must be unique across all Cloud Storage buckets:

    BUCKET_NAME="your-bucket-name"
    
Select a region

    REGION="europe-west1"
    
Create your Cloud Storage bucket in this region and, later, use the same region for training and prediction. Run the following command to create the bucket if it doesn't already exist:

    gsutil mb -l $REGION gs://$BUCKET_NAME
    
## Quickstart for training in AI Platform

Create a virtual environment that uses Python 3

    python3 -m virtualenv venv
    
Activate that environment

    source venv/bin/activate

Next, install Python dependencies needed to train the model locally:

    pip install -r requirements.txt
        
When you run the training job in AI Platform, dependencies are preinstalled based on the [runtime version](https://cloud.google.com/ai-platform/training/docs/runtime-version-list) you choose.


## Train your model locally

Before training on AI Platform, train the job locally to verify the file structure and packaging is correct.

For a complex or resource-intensive job, you may want to train locally on a small sample of your dataset to verify your code. Then you can run the job on AI Platform to train on the whole dataset.

This sample runs a relatively quick job on a small dataset, so the local training and the AI Platform job run the same code on the same data.

Run the following command to train a model locally:

    # This is similar to `python -m trainer.task --job-dir local-training-output`
    # but it better replicates the AI Platform environment, especially
    # for distributed training (not applicable here).
    gcloud ai-platform local train \
      --package-path trainer \
      --module-name trainer.task \
      --job-dir local-training-output
      
## Train your model using AI Platform

Next, submit a training job to AI Platform. This runs the training module in the cloud and exports the trained model to Cloud Storage.

First, give your training job a name and choose a directory within your Cloud Storage bucket for saving intermediate and output files. Set these as environment variables. For example:

    JOB_NAME="lab_ai_platform"
    JOB_DIR="gs://$BUCKET_NAME/keras-job-dir"
    
Run the following command to package the trainer/ directory, upload it to the specified --job-dir, and instruct AI Platform to run the trainer.task module from that package.

The --stream-logs flag lets you view training logs in your shell. You can also see logs and other job details in the Google Cloud Console.

    gcloud ai-platform jobs submit training $JOB_NAME \
      --package-path trainer/ \
      --module-name trainer.task \
      --region $REGION \
      --python-version 3.7 \
      --runtime-version 1.15 \
      --job-dir $JOB_DIR \
      --stream-logs
      
This may take longer than local training, but you can observe training progress in your shell in a similar fashion. At the end, the training job exports the trained model to your Cloud Storage bucket and prints a message like the following:

    INFO    2019-03-27 17:57:11 +0000   master-replica-0        Model exported to:  gs://your-bucket-name/keras-job-dir/keras_export/1553709421
    INFO    2019-03-27 17:57:11 +0000   master-replica-0        Module completed; cleaning up.
    INFO    2019-03-27 17:57:11 +0000   master-replica-0        Clean up finished.
    INFO    2019-03-27 17:57:11 +0000   master-replica-0        Task completed successfully.
    
In GCP Console > AI Platform > Jobs you can view your training jobs and view the logs:
    
![](images/step1.png)

![](images/step2.png)
    
 ## Hyperparameter tuning
 
 You can optionally perform hyperparameter tuning by using the included hptuning_config.yaml configuration file. This file tells AI Platform to tune the batch size and learning rate for training over multiple trials to maximize accuracy.
 Learn more about [hyperparameter tuning in AI Platform Training](https://cloud.google.com/ai-platform/training/docs/hyperparameter-tuning-overview).
 
 
    gcloud ai-platform jobs submit training ${JOB_NAME}_hpt \
      --config hptuning_config.yaml \
      --package-path trainer/ \
      --module-name trainer.task \
      --region $REGION \
      --python-version 3.7 \
      --runtime-version 1.15 \
      --job-dir $JOB_DIR \
      --stream-logs
      
 Get the results of the hyperparameters tuning
 
 ![](images/step3.png)
 
 ![](images/step4.png)
      
  ## Predictions
  
  This section shows how to use AI Platform and your trained model from the previous section to predict a person's income bracket from other Census information about them.
  
 ## Create model and version resources in AI Platform
 
 To serve online predictions using the model you trained and exported in the quickstart for training, create a model resource in AI Platform and a version resource within it. The version resource is what actually uses your trained model to serve predictions. This structure lets you adjust and retrain your model many times and organize all the versions together in AI Platform. Learn more about [models and versions](https://cloud.google.com/ai-platform/prediction/docs/projects-models-versions-jobs).
 
 First, name and create the model resource:
 
    MODEL_NAME="lab_ai_platform_model"
    
    gcloud ai-platform models create $MODEL_NAME \
      --regions $REGION
  
  ![](images/step5.png)
      
 Next, create the model version. 
 You will find the saved model of the best model in :
 gs://$BUCKET_NAME/keras-job-dir/<number of best model>/keras_export
 
     MODEL_VERSION="v1"
     
     # Get a list of directories in the `keras_export` parent directory. Then pick
     # the directory with the latest timestamp, in case you've trained multiple
     # times.
     
     SAVED_MODEL_PATH="gs://$BUCKET_NAME/<path to keras_export>"
     
     # Create model version based on that SavedModel directory
     gcloud ai-platform versions create $MODEL_VERSION \
       --model $MODEL_NAME \
       --runtime-version 1.15 \
       --python-version 3.7 \
       --framework tensorflow \
       --origin $SAVED_MODEL_PATH
 
  
  
  ![](images/step6.png)    
  
 Submit the online prediction request
 
    gcloud ai-platform predict \
      --model $MODEL_NAME \
      --version $MODEL_VERSION \
      --json-instances prediction_input.json
      
Since the model's last layer uses a sigmoid function for its activation, outputs between 0 and 0.5 represent negative predictions ("<=50K") and outputs between 0.5 and 1 represent positive ones (">50K").

## Cleaning up

    # Delete model version resource
    gcloud ai-platform versions delete $MODEL_VERSION --quiet --model $MODEL_NAME
    
    # Delete model resource
    gcloud ai-platform models delete $MODEL_NAME --quiet
    
    # Delete Cloud Storage objects that were created
    gsutil -m rm -r $JOB_DIR
    
    # If training job is still running, cancel it
    gcloud ai-platform jobs cancel $JOB_NAME --quiet
    
    # If your Cloud Storage bucket doesn't contain any other objects and you would like to delete it, run
    gsutil rm -r gs://$BUCKET_NAME



