# Vision APis

## Challenge scenario

You've been ask to compare the vision APIs from AWS and GCP.
Here is a small webapp which allow you to uplload a photo and get labels from GCP Vision API and 
You've been asked to provide the code to upload the photo to GCS and S3 and call the Vision API and Rekognition API to get labels
from your picture.


## Enable the GCP APIs

Before we can begin using the Vision, Storage, and Datastore APIs, you must enable the APIs with the following commands:

    gcloud services enable vision.googleapis.com
    gcloud services enable storage-component.googleapis.com
    gcloud services enable datastore.googleapis.com
    
## Authenticate API Requests

In order to make requests to the Vision, Storage, and Datastore APIs, you will need service account credentials. Service account credentials from your project can be generated using the gcloud tool.

    export PROJECT_ID=[YOUR_PROJECT_ID]
    
Create a Service Account to access the Google Cloud APIs when testing locally:

    gcloud iam service-accounts create yottalab --display-name "My Yotta Lab Service Account"
    
Give your newly created Service Account appropriate permissions:

    gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="serviceAccount:yottalab@${PROJECT_ID}.iam.gserviceaccount.com" --role="roles/owner"
    
After creating your Service Account, create a Service Account key:

    gcloud iam service-accounts keys create key.json --iam-account yottalab@${PROJECT_ID}.iam.gserviceaccount.com
    
This command generates a service account key stored in a JSON file named key.json in your current directory.

Using the absolute path of the generated key, set an environment variable for your service account key in the Cloud Shell:

    export GOOGLE_APPLICATION_CREDENTIALS="$(pwd)/key.json"  
    
You can read more about [authenticating the Vision API](https://cloud.google.com/vision/docs/common/auth).


## Install dependencies

Create an isolated Python 3 environment named env with virtualenv:

    python -m virtualenv -p python3 venv
    
Enter your newly created virtualenv named env:

    source venv/bin/activate
    
Use pip to install dependencies for your project from the requirements.txt file:

    pip install -r requirements.txt
    
## Create a Storage Bucket

    export CLOUD_STORAGE_BUCKET=<choose a unique name>
    gsutil mb gs://${CLOUD_STORAGE_BUCKET}
    
## Create a S3 Bucket

    export S3_BUCKET=<choose a unique name>
    aws s3 mb s3://${S3_BUCKET}
    
## Launch the application

    python main.py
    
Edit the file main.py and add code below the TODOs.

## Clean up

Don't forget to delete your S3 and GCS buckets
      