# DB - Lab

## Your challenge

Your challenge is to create a MySQL DB in the cloud, create a db (bts) and a table flights and analyze the data provided from a cloud compute instance.
We will use historical flight arrival data published by the US Bureau of Transportation Statistics (USA) and analyze it.
You will find information on the data here : https://www.transtats.bts.gov/DL_SelectFields.asp?Table_ID=236

## Create a MySql DB

Call your db "flights"

In the cloud SQL console click "Create Instance"

![](images/step1.png)

Choose a MySQL DB

![](images/step2.png)

Call your db "flights"
Choose a password for user "root"

![](images/step3.png)


or with gcloud CLI:

    gcloud sql instances create flights --tier=db-n1-standard-1 --activation-policy=ALWAYS
    
    gcloud sql instances set-root-password flights --password <PASSWD>


## Create a Compute instance

Choose a Debian or Ubuntu distribution. Make sure you will be able to request the DB from your instance.

Choose a f1.micro instance
![](images/step4.png)

In the Identity and API access choose enable Cloud SQL

![](images/step5.png)

In the Cloud SQL console for your database in "Connections" click "Add network" and enter the public IP of your instance

![](images/step6.png)

or with gcloud CLI from your instance :

    gcloud sql instances patch flights --authorized-networks $(wget -qO - http://ipecho.net/plain)

## Populate the DB from the compute instance

Connect to your instance with ssh.

Install the mysql client :

    sudo apt-get update
    sudo apt-get install mysql-client
    
    
Connect to your db :

You will find yout db IP here :

![](images/step7.png)
    
    MYSQLIP=$(gcloud sql instances describe flights --format="value(ipAddresses.ipAddress)")
    mysql --host=$MYSQLIP --user=root  --password
    
Create the database and the table :

```sql
    create database if not exists bts;
    use bts;
    drop table if exists flights;
    create table flights (
      FL_DATE date,
      UNIQUE_CARRIER varchar(16),
      AIRLINE_ID varchar(16),
      CARRIER varchar(16),
      FL_NUM integer,
      ORIGIN_AIRPORT_ID varchar(16),
      ORIGIN_SEQ_ID varchar(16),
      ORIGIN_CITY_MARKET_ID varchar(16),
      ORIGIN varchar(16),
      DEST_AIRPORT_ID varchar(16),
      DEST_AIRPORT_SEQ_ID varchar(16),
      DEST_CITY_MARKET_ID varchar(16),
      DEST varchar(16),
      CRS_DEP_TIME integer,
      DEP_TIME integer,
      DEP_DELAY float,
      TAXI_OUT float,
      WHEELS_OFF integer,
      WHEELS_ON integer,
      TAXI_IN float,
      CRS_ARR_TIME integer,
      ARR_TIME integer,
      ARR_DELAY float,
      CANCELLED float,
      CANCELLATION_CODE varchar(16),
      DIVERTED float,
      DISTANCE float,
      INDEX (FL_DATE), INDEX (ORIGIN_AIRPORT_ID),
      INDEX(ARR_DELAY), INDEX(DEP_TIME), INDEX(DEP_DELAY)
    );
```

From your computer, upload the data to your instance

    gcloud compute scp --recurse data <instance>:~
    
From your instance populate the DB;

    mysqlimport --local --host=$MYSQLIP --user=root --password --ignore-lines=1 --fields-terminated-by=',' bts flights.csv-*


## Analyze the data

    use bts;
    describe flights;
    
    select DISTINCT(FL_DATE) from flights;
    select DISTINCT(CARRIER) from flights;
    
    
    select count(dest) from flights where arr_delay < 15 and dep_delay < 15;
    select count(dest) from flights where arr_delay >= 15 and dep_delay < 15;
    select count(dest) from flights where arr_delay < 15 and dep_delay >= 15;
    select count(dest) from flights where arr_delay >= 15 and dep_delay >= 15;

## Request the DB with python :

Install python and pip

    sudo apt install python-pip
    
Install python dependencies

    pip install mysql-connector-python
    pip install pandas

From a python shell

    import mysql.connector as sql
    
    import pandas as pd
    
    conn = sql.connect(host='database-1.cf9jzprgkceo.eu-west-1.rds.amazonaws.com', database='bts', user='admin', password='xUAvd!3b7jzE3$eD')
    
    df = pd.read_sql_query("SELECT * FROM flights LIMIT 100", conn)
    df.head()

## Clean up

Don't forget to delete your instance and db!