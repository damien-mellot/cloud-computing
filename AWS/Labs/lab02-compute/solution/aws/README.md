# Storage - Lab


##  Cloud Compute

## Challenge scenario

You have been asked to test the possibility for Data Scientists in your team to use cloud compute instances as workstations.

## Your challenge

Your challenge is to launch an vm instance in the cloud and connect to a jupyter notebook running on this instance from your computer.

A data scientist has provided a test notebook "csv.ipynb".

The dataset "train.csv" and "eval.csv" will be stored on a cloud storage bucket.

# Create a Cloud Storage Bucket

And upload the dataset "eval.csv" and "train.csv"

## Create a compute instance

Create a vm instance to host a jupyter server.
Your VM should be able to access a bucket in the cloud storage service.
You should be able to connect to your instance in ssh and access the port 8888 with TCP.

(hint for AWS : 
- create an IAM Role allowing EC2 instances to access files in a S3 bucket
- create a SecurityGroup allow tcp connection on port 22 and 8888 for protocol TCP from Source 0.0.0.0/0)

(hint for GCP : 
- create an Firewall Rules allowing ssh connections on port 22 and TCP connections on port 8888 for network-tag "allow-jupyter"
- add the network tag "allow-jupyter" to your instance)

From EC2 service click on "Create Instance"

![](images/step1.png)

![](images/step2.png)

Choose an Amazon Linux AMI

![](images/step3.png)

Choose a t2.micro instance

![](images/step4.png)

Click on Create a new IAM role

![](images/step5.png)

Review storage, click on "Next"

![](images/step6.png)

Review tags, click on "Next"

![](images/step7.png)

Configure Security Group
Add a Rule for TCP, Port 888, From Anywhere
Click on "Review and Launch"

![](images/step8.png)

Review, click on Launch
![](images/step9.png)

Create a new key pair (don't forget to download the pem file) and Launch
![](images/step10.png)


![](images/step11.png)


## Host a jupyter server

Connect to your instance with ssh and install a jupyter server.

Connect to your instance with ssh on a new browser tab

![](images/step12.png)

Here is the procedure to install jupyter on a new machine :

```bash
# install Miniconda as your python distribution : 
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh
bash ~/miniconda.sh

# activate the base conda env
conda activate

# install python librairies needed e.g.:
pip install tensorflow --no-cache-dir
pip install matplotlib
pip install pandas
pip install pillow
pip install jupyterlab

# for AWS
pip install boto3

# for GCP
pip install google-cloud-storage

# launch jupyter on port 8888 open to external connections
# (see https://jupyter-notebook.readthedocs.io/en/stable/public_server.html#notebook-public-server) :
jupyter notebook --port=8888 --no-browser --ip=0.0.0.0 &
```

## Test the jupyter notebook

Try to access the jupyter notebook from your browser.
http://<INSTANCE-EXTERNAL-IP>:8888/?token=<TOKEN>

Click on the instance name in GCE VM Instances page and note the External IP.
![](images/step6.png)

![](images/step10.png)


(The token is displayed in your ssh console when launching jupyter)

Visit http://<INSTANCE-EXTERNAL-IP>:8888/lab for JupyterLab

Drag and drop "csv.ipynb" in Jupyter Lab file browser.


Execute all cells from the notebook.
Replace BUCKET_NAME with the name of your bucket.

![](images/step11.png)

## Bonus

Try to redo this exercise using only the CLI for your favorite cloud platform.


```bash
aws ec2 create-key-pair --key-name MyKeyPair --query 'KeyMaterial' --output text > MyKeyPair.pem


chmod 400 MyKeyPair.pem


# Create an IAM Role and instance profile

# creat policy.json file
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}


aws iam create-role --role-name S3FullAccess --assume-role-policy-document file://policy.json

# give full S3 access to the newly created role
aws iam attach-role-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --role-name S3FullAccess

aws iam create-instance-profile --instance-profile-name S3FullAccess

aws iam add-role-to-instance-profile --instance-profile-name S3FullAccess --role-name S3FullAccess


# create a security group which allow ssh and tcp connection on port 8888 (default jupyter port)
aws ec2 create-security-group --group-name MySecurityGroup --description "My security group" --vpc-id vpc-XXXXXXXX

aws ec2 authorize-security-group-ingress \
    --group-id sg-XXXXXXXXXXXXXXXXXX \
    --protocol tcp \
    --port 8888 \
    --cidr 0.0.0.0/0
    
aws ec2 authorize-security-group-ingress \
    --group-id sg-XXXXXXXXXXXXXXXXXX \
    --protocol tcp \
    --port 22 \
    --cidr 0.0.0.0/0
    
# create a key pair
aws ec2 create-key-pair --key-name MyKeyPair --query 'KeyMaterial' --output text > MyKeyPair.pem
    
# get the latest amazon linux image id
aws ec2 describe-images --owners amazon --filters 'Name=name,Values=amzn-ami-hvm-????.??.?.????????-x86_64-gp2' 'Name=state,Values=available' --query 'reverse(sort_by(Images, &CreationDate))[:1].ImageId' --output text
    
# get a subnet id
aws ec2 describe-subnets

aws ec2 run-instances --image-id ami-028188d9b49b32a80 --count 1 --instance-type t2.micro --key-name MyKeyPair --security-group-ids sg-XXXXXXXXXXXXXXXXXX --subnet-id subnet-XXXXXXXX --iam-instance-profile "Name=S3FullAccess"


ssh ec2-user@<EXTERNAL-IP> -i MyKeyPair.pem

aws ec2 terminate-instances --instance-ids i-XXXXXXXXXXXXXXXXXX
```

## Delete the bucket and the compute instance!

Don't forget to delete the bucket and the compute instance once you're done with the lab!

![](images/step13.png)