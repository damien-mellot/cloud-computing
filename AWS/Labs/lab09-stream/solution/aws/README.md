## Create a Kinesis Data Firehose Delivery Stream

Navigate to the Kinesis service.
Click Get started.

![](images/step1.png)


Click Create Delivery Stream to create a Kinesis Data Firehose stream.

![](images/step2.png)

Enter "captains-kfh" as the Delivery stream name.

![](images/step3.png)

Click Next

![](images/step4.png)


Select Amazon S3 as the destination.

For the S3 Bucket, click Create New.

Enter a globally unique bucket name, starting with "kfh-ml".

Click Create S3 Bucket -> Click Next.

![](images/step5.png)

Enter "1" MB as the Buffer size -> Enter "60" seconds as the Buffer interval.

Click Create new for the IAM role.

![](images/step6.png)

Click Allow -> Click Next -> Click Create delivery stream.

![](images/step7.png)

## Stream Data to the New Kinesis Data Firehose Delivery Stream


On your local machine make sure you have a python environment with boto3 and faker installed:

pip install boto3
pip install faker

Run the following command.

        python write-to-kinesis-firehose-space-captains.py
    

Check that data are processed by you Kinesis Firehose in the monitoring tab.

![](images/step8.png)

Navigate to your S3 bucket destination from the "Details" page

![](images/step9.png)

Refresh the view on the S3 bucket every 30 seconds and wait for records to appear in the S3 bucket. It may take 60 seconds for them to show.

![](images/step10.png)
        
## Create a Kinesis Data Analytics Application

Navigate to the Kinesis service in the AWS Management Console.

Click Data Analytics on the left-side menu -> Click Create application.

![](images/step11.png)

Enter "popular-space-captains" as the Application name.

Enter "popular-space-captains" as the Description.

Ensure the SQL runtime is selected.

Click Create application.

Click Connect streaming data.

![](images/step12.png)

Ensure Choose source is selected at the top.

Select Kinesis Firehose delivery stream as the Source.

Choose the captains-kfh created earlier as the Kinesis Firehose delivery stream.

![](images/step13.png)

Click Discover schema.

![](images/step14.png)

Click Save and continue.

![](images/step15.png)

Click Go to SQL editor -> Click Yes, start application.

![](images/step16.png)

This can take a long time, browse the SQL Template until your application is ready.

![](images/step17.png)

Copy the SQL code from the kinesis-analytics-popular-captains.sql file and paste it into the SQL editor.

Click Save and run SQL.

![](images/step18.png)


View the real-time analytics DESTINATION_CAPTAINS_SCORES results.

![](images/step19.png)

## Create a Kinesis Data Analytics Anomaly Detection Application


Copy the SQL code from the kinesis-analytics-rating-anomaly.sql file and paste it into the SQL editor.

Click Save and run SQL.

View the real-time analytics DESTINATION_SQL_STREAM results.

![](images/step20.png)

## Clean up

Don't forget to delete your Kinesis Data Analytics application, your Kinesis Data Firehose and your S3 bucket



