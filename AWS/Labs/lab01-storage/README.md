# Storage - Lab


##  Cloud Storage

Digital objects — images, videos, documents, etc. — are a key aspect of any type of computing, whether local or cloud-based. 
Cloud Storage is a central facility for storing and retrieving your binary objects and 
is therefore utilized by the vast majority of applications on a cloud platform. 
In this hands-on lab, you’ll gain practical experience in creating and managing the primary component of Storage in the Cloud, 
the bucket, as well as manipulating the objects it holds, through both the console and the command line.

## Challenge scenario

Your company is ready to launch a brand new product! Because you are entering a totally new space, 
you have decided to deploy a new static website as part of the product launch. The new site is complete, 
but the person who built the new site left the company before they could deploy it.

## Your challenge

Your challenge is to deploy the site in the public cloud by completing the tasks below. 
You will use a the website in the directory static-website as a placeholder for the new site in this exercise. Good luck!

## Create a bucket

Create a bucket, name it with a globally unique name.

## Host a the static website

Upload any the static website code available in the static-website folder in your newly created bucket.

## Share the file

Modify the bucket and file permissions to allow anyone to access all objects.

## Test the static website

Try to access the static website publicly by reaching the public URL of the index.html in your bucket.

## Bonus

Try to redo this exercise using only the CLI for your favorite cloud platform.

Try to enable versioning in your bucket and upload a new version of the static website.

## Delete the bucket

Don't forget to delete the bucket once you're done with the lab!
